﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ponti.ClassLib.Manager;
using Ponti.ClassLib.Models;
using Ponti.ClassLib.Views;

namespace Ponti.ClassLib.Presenter
{
    public class PresenterMainForm
    {
        private IViewFormMain m_view;

        public PresenterMainForm(IViewFormMain view)
        {
            m_view = view;
        }

        public Task LoadDataSource()
        {
            return Task.Factory.StartNew(() => {
                                            Console.WriteLine("LoadDataSource presenter taskid={0}",Task.CurrentId);
                                            ModelManager manager = new ModelManager();
                                            var list = manager.GetSusers();
                                            m_view.setData(list);
            });  
        }



        public Task SetSuser(String username, String usercd,String oldUsername,String oldUsercd)
        {
            return Task.Factory.StartNew(() =>
            {

                Console.WriteLine("SetSuser presenter taskid={0}", Task.CurrentId);
                ModelManager manager = new ModelManager();
                manager.SetSuser(username, usercd,oldUsername,oldUsercd);
            });
        }
    }
}
