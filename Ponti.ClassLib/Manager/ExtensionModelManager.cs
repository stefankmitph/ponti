﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Ponti.ClassLib.Models;

namespace Ponti.ClassLib.Manager
{
    public static class ExtensionModelManager
    {
        /*
         * extends GetSuser with parameters 
         * returns the susers selected by fircd and eart 
         */
        public static List<SUSER> GetSusers(this ModelManager manager , string fircd,string eart)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GodClass.ConnectionString))
                {

                    return connection.Query<SUSER>("SELECT * FROM SUSER " +
                                                   "WHERE (FIRCD = @FIRCD) AND (EART = @EART)"
                                                   , new { FIRCD = fircd , EART = eart}
                                                   ).ToList();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("*peeep* euch, *peeep* euch alle.");
            }
            return null;
        }

        /* 
         */
        public static void SetSuser(this ModelManager manager,  string username,string usercd,String oldUsername,String oldUsercd)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GodClass.ConnectionString))
                {
                    Console.WriteLine("setting suser in db");
                     connection.Execute("UPDATE SUSER " +
                                        "SET USERCD = @USERCD, USERNAME = @USERNAME "+
                                        "WHERE (USERCD = @OLDUSERCD) AND (USERNAME = @OLDUSERNAME) "
                                        , new { USERCD = usercd, USERNAME = username,OLDUSERCD = oldUsercd , OLDUSERNAME = oldUsername}
                                        );
                }
            }
            catch (Exception)
            {
                Console.WriteLine("SetSuser Exception");
            }
           
        }


        /* 
 */
        public static void InsertSuser(this ModelManager manager, string username, string usercd, String fircd, String eart)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GodClass.ConnectionString))
                {
                    Console.WriteLine("inserting new suser");

                    connection.Execute("INSERT INTO SUSER " +
                                       "( FIRCD , EART , USERCD , USERNAME ) " +
                                       "VALUES ( @FIRCD , @EART , @USERCD , @USERNAME ) "
                                       , new { FIRCD = fircd, EART = eart, USERCD = usercd,USERNAME = username  }
                                       );
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Insert Exception");
            }

        }


        public static void DeleteSuser(this ModelManager manager, string username, string usercd, String fircd, String eart)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(GodClass.ConnectionString))
                {
                    Console.WriteLine("delete suser");

                    connection.Execute("DELETE FROM SUSER " +
                                       "WHERE (FIRCD =@FIRCD) AND (EART = @EART) AND (USERCD = @USERCD) AND (USERNAME = @USERNAME)" 
                                       , new { FIRCD = fircd, EART = eart, USERCD = usercd, USERNAME = username }
                                       );
                }
            }
            catch (Exception)
            {
                Console.WriteLine("delete Exception");
            }

        }
    }
}
