﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ponti.ClassLib.Models
{
    public class SUSER
    {
        public string FIRCD { get; set; }
        public string EART { get; set; }
        public string USERCD { get; set; }
        public string USERNAME { get; set; }
    }
}
