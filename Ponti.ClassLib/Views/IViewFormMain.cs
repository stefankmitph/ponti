﻿using System.Collections.Generic;
using System.Windows;
using Ponti.ClassLib.Models;

namespace Ponti.ClassLib.Views
{
    public interface IViewFormMain
    {
         void setData(List<SUSER> l);
    }
}