﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Ponti.ClassLib;
using Ponti.ClassLib.Manager;
using Ponti.ClassLib.Models;
using Ponti.ClassLib.Presenter;
using Ponti.ClassLib.Views;


namespace Ponti.Forms
{
    public partial class FormMain : Form, IViewFormMain
    {
        private readonly PresenterMainForm m_presenter;
        private int rowindex;
        private bool updateDb;
        private String oldUsername;
        private String oldUsercd;

        public FormMain()
        {
            m_presenter = new PresenterMainForm(this);
            updateDb = false;
            rowindex = 0;
            oldUsername = null;
            oldUsercd = null;
            InitializeComponent();
        }

        private void FormMainLoad(object sender, EventArgs e)
        {
            //LoadDataSource();
            try
            {
                LoadAsyncDataSource();
                
            
                /* LoadAsyncDataSource soll den DataSource hinzufügen, so wie eh jetzt auch,
                 * aber nicht einfach über datagridview.datasource = list<>, sondern:
                 * datagridview.datasource = new bindingsource(list<>, null)
                 * den datasource dann aus LoadAsyncDataSource returnieren und ins InitializeDataBindings schießen
                 */
                InitializeDataBindings(null);//(BindingSource)dataGridView.DataSource
                
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString());}
        }


        

        private void InitializeDataBindings(BindingSource bindingSource)
        {
            /* jedes objekt, das von der klasse control abgeleitet is, hat eine eigenschaft 'databindings'
             * control.Databindings.Add(...) fügt dem control das binding hinzu, d.h.
             * es checkt, wenn sich am datasource was geändert hat
             * bitte für beide textboxen usercd und username hinzufügen
             */
            /*
            if(bindingSource == null)
                Console.WriteLine("\nbinding source is null");
            */

            /*
            this.textBoxUsercd.DataBindings.Add("Text", bindingSource.DataSource, "USERCD");
            this.textBoxUsername.DataBindings.Add("Text", bindingSource.DataSource, "USERNAME");
            */

        }

        private async void LoadAsyncDataSource()
        {
            await m_presenter.LoadDataSource();
        }

        [Obsolete]
        private void LoadDataSourceModelManager()
        {
            ModelManager manager = new ModelManager();

            var list = manager.GetSusers("ESRZ", "S");

            if (list != null)
                dataGridView.DataSource = list;
            else
                 throw new Exception("List is NULL");            
        }
               
        [Obsolete]
        private void LoadDataSource()
        {
            DataSet myDataSet = new DataSet();
            SqlConnection oleDbConnection = null;
            try
            {
                oleDbConnection = new SqlConnection(GodClass.ConnectionString);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Failed to create a database connection. \n{0}", ex.Message);
                return;
            }

            try
            {

                SqlCommand myCommand = new SqlCommand("SELECT * FROM SUSER", oleDbConnection);
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);

                oleDbConnection.Open();
                myDataAdapter.Fill(myDataSet, "SUSER");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Failed to retrieve the required data from the DataBase.\n{0}", ex.Message);
                return;
            }
            finally
            {
                oleDbConnection.Close();
            }

            dataGridView.DataSource = myDataSet.Tables["SUSER"];
        }

        private void DataGridViewRowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            /* Wenn der Benutzer auf den RowHeader (bei uns heißt er schneidig 'Datensatzmarkierer') clickt,
             * dann sollen die TextBoxen aktiv werden
             * erst wenn sich tatsächlich was geändert hat, soll buttonSave enabled werden (event: textchanged)
             */
            this.buttonSave.Enabled = false;
            
            if (updateDb)
            {
                if (oldUsername != null)
                {
                    dataGridView.Rows[rowindex].Cells["USERNAME"].Value = oldUsername;
                    oldUsername = null;
                }

                if (oldUsercd != null)
                {
                    dataGridView.Rows[rowindex].Cells["USERCD"].Value = oldUsercd;
                    oldUsercd = null;
                } 
            }

            rowindex = e.RowIndex;
            updateDb = false;
        }




        private void UsernameLeave(object sender, EventArgs e)
        {
            if (dataGridView.Rows[rowindex].Cells["USERNAME"].Value == null)
            {
                return;
            }
            String tbox = textBoxUsername.Text;
            String gv = dataGridView.Rows[rowindex].Cells["USERNAME"].Value.ToString();
            
            if (!tbox.Equals(gv))
            {
                buttonSave.Enabled = true;
                oldUsername = dataGridView.Rows[rowindex].Cells["USERNAME"].Value.ToString();
                updateDb = true;
            }

        }

        private void UsercdLeave(object sender, EventArgs e)
        {
            if (dataGridView.Rows[rowindex].Cells["USERCD"].Value == null)
            {
                return;
            }
            String tbox = textBoxUsercd.Text;
            String gv =dataGridView.Rows[rowindex].Cells["USERCD"].Value.ToString();
            
            if (!tbox.Equals(gv))
            {
                buttonSave.Enabled = true ;
                oldUsercd = dataGridView.Rows[rowindex].Cells["USERCD"].Value.ToString();
                updateDb = true;
            }
        }

        private void usercd_TextChanged(object sender, EventArgs e)
        {
        }

        private void ButtonSaveClick(object sender, EventArgs e)
        {
            /* änderungen in die datenbank kleschen
             */
            if (updateDb)
            {
                String username = textBoxUsername.Text;
                String usercd = textBoxUsercd.Text;

                SetSuser(username, usercd, oldUsername, oldUsercd);
                
                buttonSave.Enabled = false;
                updateDb = false;
                oldUsername = null;
                oldUsercd = null;
            }
        }


        private async void SetSuser(String username,String usercd, String oldUsername,String oldUsercd)
        {
            await m_presenter.SetSuser(username,usercd,oldUsername,oldUsercd);
        }

        public void setData(List<SUSER> l)  
        {

            if (dataGridView.InvokeRequired)
            {
                //dataGridView.DataSource = new BindingSource(l, null);
                dataGridView.Invoke(new MethodInvoker(delegate { dataGridView.DataSource = new BindingSource(l, null); }));
                
                dataGridView.Invoke(new MethodInvoker(
                   delegate { textBoxUsercd.DataBindings.Clear(); }));

                dataGridView.Invoke(new MethodInvoker(
                   delegate { textBoxUsername.DataBindings.Clear(); }));
                

                // warum auch immer, aber da gehts binding ...
                
                dataGridView.Invoke(new MethodInvoker(
                    delegate { this.textBoxUsercd.DataBindings.Add("Text", dataGridView.DataSource, "USERCD"); }));

                dataGridView.Invoke(new MethodInvoker(
                   delegate { this.textBoxUsername.DataBindings.Add("Text", dataGridView.DataSource, "USERNAME"); }));
               

            }
        }
    }
}
