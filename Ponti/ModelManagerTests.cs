﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ponti.ClassLib.Manager;
using Ponti.ClassLib.Models;

namespace Ponti
{
    [TestClass]
    public class ModelManagerTests
    {
        [TestMethod]
        public void GetAllModelsTest()
        {
            ModelManager manager = new ModelManager();
            var list = manager.GetSusers();
            Assert.IsNotNull(list);
        }

        /* 
         * tests parameterized extension of GetUsers()
         */
        [TestMethod]
        public void GetUsersByFircdTest()
        {
            ModelManager manager = new ModelManager();
            var list = manager.GetSusers("ESRZ", "S");
            Assert.IsNotNull(list);
        }


        /*
         * (SUCCESS) ? "USE YOUR OWN CONNECTION STRING IN GODCLASS" : "HAV"
         */
        [TestMethod]
        public void FailDbWrongConnectionString()
        {
            ModelManager manager = new ModelManager();
            var list = manager.GetSusers();
            Assert.IsNull(list);
        }

        [TestMethod]
        public void DeleteInsertTest()
        {
            String fircd = "test";
            String eart = "T";
            String username = "u_test";
            String usercd = "u_test";
            
            ModelManager manager = new ModelManager();
            manager.DeleteSuser(username,usercd,fircd,eart);
            manager.InsertSuser(username,usercd,fircd,eart);

            var list = manager.GetSusers(fircd, eart);
            Console.WriteLine(list.Count);
            Assert.IsTrue(list.Count==1);
        }

        [TestMethod]
        public void UpdateTest()
        {
            String fircd = "test";
            String eart = "G";
            String username = "us_test";
            String usercd = "us_test";

            ModelManager manager = new ModelManager();
            manager.DeleteSuser(username, usercd, fircd, eart);
            manager.InsertSuser(username, usercd, fircd, eart);
            manager.SetSuser("new","new",username,usercd);

            var list = manager.GetSusers(fircd, eart);
            
            Console.WriteLine(list.Count);
            Assert.IsTrue(list.Count == 1);
        }
    }
}
